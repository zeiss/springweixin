package com.entity;

import java.sql.Timestamp;

/**
 * Videos entity. @author MyEclipse Persistence Tools
 */

public class Videos implements java.io.Serializable {

    // Fields

    private Integer id;
    private Mediaid mediaid;
    private User user;
    private String title;
    private String description;
    private Timestamp ctime;

    // Constructors

    /** default constructor */
    public Videos() {
    }

    /** full constructor */
    public Videos(Mediaid mediaid, User user, String title, String description,
	    Timestamp ctime) {
	this.mediaid = mediaid;
	this.user = user;
	this.title = title;
	this.description = description;
	this.ctime = ctime;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Mediaid getMediaid() {
	return this.mediaid;
    }

    public void setMediaid(Mediaid mediaid) {
	this.mediaid = mediaid;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescription() {
	return this.description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Timestamp getCtime() {
	return this.ctime;
    }

    public void setCtime(Timestamp ctime) {
	this.ctime = ctime;
    }

}