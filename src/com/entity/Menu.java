package com.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Menu entity. @author MyEclipse Persistence Tools
 */

public class Menu implements java.io.Serializable {

    // Fields

    private Integer id;
    private Menu menu;
    private String text;
    private String iconCls;
    private Boolean checked;
    private String url;
    private Boolean st;
    private String description;
    private Boolean action;
    private String role;
    private Set menus = new HashSet(0);

    // Constructors

    /** default constructor */
    public Menu() {
    }

    /** minimal constructor */
    public Menu(String text) {
	this.text = text;
    }

    /** full constructor */
    public Menu(Menu menu, String text, String iconCls, Boolean checked,
	    String url, Boolean st, String description, Boolean action,
	    String role, Set menus) {
	this.menu = menu;
	this.text = text;
	this.iconCls = iconCls;
	this.checked = checked;
	this.url = url;
	this.st = st;
	this.description = description;
	this.action = action;
	this.role = role;
	this.menus = menus;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Menu getMenu() {
	return this.menu;
    }

    public void setMenu(Menu menu) {
	this.menu = menu;
    }

    public String getText() {
	return this.text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public String getIconCls() {
	return this.iconCls;
    }

    public void setIconCls(String iconCls) {
	this.iconCls = iconCls;
    }

    public Boolean getChecked() {
	return this.checked;
    }

    public void setChecked(Boolean checked) {
	this.checked = checked;
    }

    public String getUrl() {
	return this.url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public Boolean getst() {
	return this.st;
    }

    public void setst(Boolean st) {
	this.st = st;
    }

    public String getDescription() {
	return this.description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Boolean getAction() {
	return this.action;
    }

    public void setAction(Boolean action) {
	this.action = action;
    }

    public String getRole() {
	return this.role;
    }

    public void setRole(String role) {
	this.role = role;
    }

    public Set getMenus() {
	return this.menus;
    }

    public void setMenus(Set menus) {
	this.menus = menus;
    }

}