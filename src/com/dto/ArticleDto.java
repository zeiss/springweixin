package com.dto;

import java.sql.Timestamp;
/**
 * ArticleDto.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午12:57:34 
 * @version
 * @user micrxdd
 */
public class ArticleDto {
    private Integer id;
    private String username;
    private String title;
    private Timestamp ctime;
    private Boolean show;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Timestamp getCtime() {
        return ctime;
    }
    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }
    public Boolean getShow() {
        return show;
    }
    public void setShow(Boolean show) {
        this.show = show;
    }
    
}
