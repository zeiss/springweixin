package com.dto;
public class NewsDto {
    private Integer id;
    private String title;
    private String description;
    private String picUrl;
    private String url;
    private Integer articleid;
    private Integer itemid;
    
    public Integer getItemid() {
        return itemid;
    }
    public void setItemid(Integer itemid) {
        this.itemid = itemid;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getPicUrl() {
        return picUrl;
    }
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public Integer getArticleid() {
        return articleid;
    }
    public void setArticleid(Integer articleid) {
        this.articleid = articleid;
    }
    
}
