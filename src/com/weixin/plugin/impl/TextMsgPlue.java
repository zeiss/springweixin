package com.weixin.plugin.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.dao.NewsDao;
import com.weixin.msg.TextMsg;
import com.weixin.plugin.MsgPlugin;
import com.weixin.util.WeixinMsg;

@Component("textMsgPlue")
public class TextMsgPlue implements MsgPlugin {
    @Resource
    private NewsDao newsDao;
    private final static Logger _log = Logger.getLogger(TextMsgPlue.class);

    public TextMsgPlue() {
	// TODO Auto-generated constructor stub
	_log.info(this + "    init");
    }
    @Override
    public Object returnMsg(Map<String, String> msg) {
	// TODO Auto-generated method stub
	System.out.println(newsDao);
	TextMsg msg1 = new TextMsg();
	msg1.setFromUserName(msg.get(WeixinMsg.ToUserName));
	msg1.setToUserName(msg.get(WeixinMsg.FromUserName));
	msg1.setContent(msg.get(WeixinMsg.Content) + "此消息由插件发送");
	return msg1;
    }

}
