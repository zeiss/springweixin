package com.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.MsgprsDao;
import com.dao.TextroleDao;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.dto.TextRoleDto;
import com.entity.Msgprs;
import com.entity.Textrole;
import com.service.TextRoleService;
@Service("textRoleService")
public class TextRoleServiceImpl implements TextRoleService {
    @Resource
    private TextroleDao textroleDao;
    @Resource
    private MsgprsDao msgprsDao;
    @Override
    public Pagers TextRoleList(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	Pagers pagers=textroleDao.getForPage(pageinfo);
	List<TextRoleDto> dtos=new ArrayList<TextRoleDto>();
	List<Textrole> textroles=(List<Textrole>) pagers.getRows();
	for (Textrole textrole : textroles) {
	    TextRoleDto dto=new TextRoleDto();
	    BeanUtils.copyProperties(textrole, dto,"msgprs");
	    dto.setPrsid(textrole.getMsgprs().getId());
	    dto.setPrsname(textrole.getMsgprs().getName());
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }
    @Override
    public void SaveTextRoles(List<TextRoleDto> dtos) {
	// TODO Auto-generated method stub
	for (TextRoleDto msgRoleDto : dtos) {
	    Textrole textrole=new Textrole();
	    BeanUtils.copyProperties(msgRoleDto, textrole,"id");
	    textrole.setMsgprs(msgprsDao.findById(msgRoleDto.getPrsid()));
	    textroleDao.save(textrole);
	}
    }
    @Override
    public void UpdateTextRoles(List<TextRoleDto> dtos) {
	// TODO Auto-generated method stub
	for (TextRoleDto textRoleDto : dtos) {
	    if(textRoleDto.getId()<0){
		Textrole textrole=new Textrole();
		    BeanUtils.copyProperties(textRoleDto, textrole,"id");
		    textrole.setMsgprs(msgprsDao.findById(textRoleDto.getPrsid()));
		    textroleDao.save(textrole);
	    }else{
		Textrole textrole=textroleDao.findById(textRoleDto.getId());
		Msgprs msgprs=msgprsDao.findById(textRoleDto.getPrsid());
		BeanUtils.copyProperties(textRoleDto, textrole,"id");
		textrole.setMsgprs(msgprs);
	    }
	}
    }
    @Override
    public void DelTextRoles(String ids) {
	// TODO Auto-generated method stub
	textroleDao.DelByStringids(ids);
    }
    @Override
    public void DelTextRoles(Integer[] ids) {
	// TODO Auto-generated method stub
	textroleDao.DelByIntids(ids);
    }
}
