package com.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dao.ConfigDao;
import com.entity.Config;
import com.service.ConfigService;

/**
 * ConfigServiceImpl.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月20日 下午1:58:32 
 * micrxdd
 * 
 */
@Service("configService")
public class ConfigServiceImpl implements ConfigService{
    @Resource
    private ConfigDao configDao;
    private Map<String,String> map=new HashMap<String,String>();

    @Override
    public void initConfig() {
	// TODO Auto-generated method stub
	List<Config> configs=configDao.findAll();
	for (Config config : configs) {
	    map.put(config.getStrkey(), config.getStrvalue());
	}
    }

    @Override
    public String findUrl() {
	// TODO Auto-generated method stub
	String url= map.get("weburl");
	if(url==null||url.equals("")){
	    return "http://localhost/";
	}
	return url;
    }

    @Override
    public void Update(String key, String value) {
	// TODO Auto-generated method stub
	Config config= configDao.findone(key);
	if(config==null){
	    SaveConfig(key, value);
	    return;
	}
	config.setStrvalue(value);
    }

    @Override
    public void SaveConfig(String key, String value) {
	// TODO Auto-generated method stub
	Config config=new Config();
	config.setStrkey(key);
	config.setStrvalue(value);
	configDao.save(config);
    }
    

}
