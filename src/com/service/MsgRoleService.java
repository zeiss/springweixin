package com.service;

import java.util.List;

import com.dto.MsgRoleDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
/**
 * 消息规则服务接口
 * MsgRoleService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:19:00 
 * @version
 * @user micrxdd
 */
public interface MsgRoleService {
    /**
     * 返回消息规则列表
     * @param pageinfo
     * @return
     */
    public Pagers MsgRoleList(Pageinfo pageinfo);
    /**
     * 保存消息规则
     * @param dtos
     */
    public void SaveMsgRoles(List<MsgRoleDto> dtos);
    /**
     * 更新消息规则
     * @param dtos
     */
    public void UpdateMsgRoles(List<MsgRoleDto> dtos);
    /**
     * 通过String ids(ids=1,2,3)来进行删除操作
     * @param ids
     */
    public void DelMsgRoles(String ids);
    /**
     * 通过数组 ids(ids=1&ids=2&ids=3)来进行删除操作
     * @param ids
     */
    public void DelMsgIntRoles(Integer[] ids);
    
}
