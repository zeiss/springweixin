package com.service;

import java.util.List;

import com.dto.ArticleTypeDto;
import com.dto.Pageinfo;
import com.entity.Articletype;
import com.vo.ArticleTypeVo;
/**
 * 文章分类服务接口
 * ArticleTypeService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午3:29:03 
 * @version
 * @user micrxdd
 */
public interface ArticleTypeService {
    /**
     * 获取所有文章分类列表
     * @return
     */
    public List<ArticleTypeDto> ArticleTypeList();
    /**
     * 保存一个文章分类
     * @param articleTypeVo
     */
    public void SaveArticleType(ArticleTypeVo articleTypeVo);
    /**
     * 更新一个文章分类
     * @param articleTypeVo
     */
    public void UpdateArticleType(ArticleTypeVo articleTypeVo);
    /**
     * 根据id删除一个文章分类
     * @param id
     */
    void DelArticleType(Integer id);
    /**
     * 根据id返回这个文章分类
     * @param id
     * @return
     */
    public Object ArticleTyppById(Integer id);
    /**
     * 获取根分类  首页调用
     * @param pageinfo
     * @return
     */
    public List<Articletype> ArticleTypeRootList(Pageinfo pageinfo);
}
