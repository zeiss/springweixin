package com.service;

import java.util.Map;

/**
 * 消息处理接口 MsgService.java
 * 
 * @author microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:56:09
 * @version
 * @user micrxdd
 */
public interface MsgService {
    /**
     * 当接收到消息
     * @param msg
     * @return
     */
    public Object onMsgReveived(Map<String, String> msg);
    /**
     * 初始化消息
     */
    public void MsgRolesInit();
    /**
     * 直接获取String的原始消息
     * @param msg
     * @return
     */
    public Object onMsgReveived(String msg);
}
