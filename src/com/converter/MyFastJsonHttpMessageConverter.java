package com.converter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
/**
 * 
 * MyFastJsonHttpMessageConverter.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午12:44:46 
 * @version
 * @user micrxdd
 */
public class MyFastJsonHttpMessageConverter extends FastJsonHttpMessageConverter{

    @Override
    protected Object readInternal(Class<? extends Object> clazz,
	    HttpInputMessage inputMessage) throws IOException,
	    HttpMessageNotReadableException {
	ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
        int i; 
        while ((i = inputMessage.getBody().read()) != -1) { 
            baos.write(i); 
        } 
        return JSON.parseArray(baos.toString(), clazz); 
    }
    
}
